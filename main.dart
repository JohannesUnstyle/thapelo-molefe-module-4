import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:my_app/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Home",
      home: AnimatedSplashScreen(
        duration: 3000,
        splash: Icons.home,
        backgroundColor: Colors.deepOrange,
        splashTransition: SplashTransition.rotationTransition,
        nextScreen: Login(),
      ),
      theme: ThemeData(
          primarySwatch: Colors.blue,
          scaffoldBackgroundColor: Colors.deepOrange),
    );
  }
}
